# DocumentsApi::ErrorModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **Integer** |  | 
**message** | **String** |  | 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::ErrorModel.new(code: null,
                                 message: null)
```


