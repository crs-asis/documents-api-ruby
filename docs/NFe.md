# DocumentsApi::NFe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**versao** | **String** | Versão da NFe | [optional] 
**ch_n_fe** | **String** | Chave da NFe | [optional] 
**dh_emi** | **DateTime** | Data e hora da emissão | [optional] 
**c_nf** | **String** | cNF | [optional] 
**emit** | [**Empresa**](.md) |  | [optional] 
**dest** | [**Empresa**](.md) |  | [optional] 
**produtos** | [**Array&lt;NFeProduto&gt;**](NFeProduto.md) | Produtos | [optional] 
**eventos** | [**Array&lt;NFeEvento&gt;**](NFeEvento.md) | Eventos | [optional] 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::NFe.new(versao: null,
                                 ch_n_fe: null,
                                 dh_emi: null,
                                 c_nf: null,
                                 emit: null,
                                 dest: null,
                                 produtos: null,
                                 eventos: null)
```


