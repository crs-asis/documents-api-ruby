# DocumentsApi::NFeEvento

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tp_evento** | **Integer** |  | [optional] 
**x_motivo** | **String** |  | [optional] 
**x_evento** | **String** |  | [optional] 
**dh_reg_evento** | **DateTime** |  | [optional] 
**dh_evento** | **DateTime** |  | [optional] 
**c_stat** | **Integer** | Código de retorno do evento | [optional] 
**n_seq_evento** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::NFeEvento.new(tp_evento: null,
                                 x_motivo: null,
                                 x_evento: null,
                                 dh_reg_evento: null,
                                 dh_evento: null,
                                 c_stat: null,
                                 n_seq_evento: null)
```


