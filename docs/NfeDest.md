# DocumentsApi::NfeDest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cnpj** | **String** |  | [optional] 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::NfeDest.new(cnpj: null)
```


