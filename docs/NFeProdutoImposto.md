# DocumentsApi::NFeProdutoImposto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**v_tot_trib** | **Float** | Valor total tributado | [optional] 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::NFeProdutoImposto.new(v_tot_trib: null)
```


