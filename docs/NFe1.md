# DocumentsApi::NFe1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dest** | [**NfeDest**](NfeDest.md) |  | [optional] 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::NFe1.new(dest: null)
```


