# DocumentsApi::Error

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **Integer** |  | 
**message** | **String** |  | 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::Error.new(code: null,
                                 message: null)
```


