# DocumentsApi::Produto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**c_prod** | **Integer** | Código do produto | [optional] 
**c_ean** | **String** | Código de barras EAN | [optional] 
**x_prod** | **String** | Nome do Produto | [optional] 
**ncm** | **String** | Nomenclatura Comum do Mercosul | [optional] 
**cest** | **Integer** | Código Especificador da Substituição Tributária | [optional] 
**cfop** | **String** | Código Fiscal de Operações e Prestações | [optional] 
**u_com** | **String** | Unidade de medida comercial | [optional] 
**q_com** | **Float** | Quantidade Comercial | [optional] 
**v_un_com** | **String** | Valor Unitário Comercial | [optional] 
**v_prod** | **Float** | Valor total dos produtos | [optional] 
**c_ean_trib** | **String** | Código de barras EAN | [optional] 
**u_trib** | **String** | Unidade de medida tributária | [optional] 
**q_trib** | **Float** | Quantidade tributada | [optional] 
**v_un_trib** | **Float** | Valor unitário tributado | [optional] 
**v_outro** | **Float** | Outras despesas acessórias | [optional] 
**ind_tot** | **Integer** | Indica se o valor do item é computado no valor final da NFe | [optional] 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::Produto.new(c_prod: null,
                                 c_ean: null,
                                 x_prod: null,
                                 ncm: null,
                                 cest: null,
                                 cfop: null,
                                 u_com: null,
                                 q_com: null,
                                 v_un_com: null,
                                 v_prod: null,
                                 c_ean_trib: null,
                                 u_trib: null,
                                 q_trib: null,
                                 v_un_trib: null,
                                 v_outro: null,
                                 ind_tot: null)
```


