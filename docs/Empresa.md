# DocumentsApi::Empresa

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cnpj** | **String** |  | [optional] 
**x_nome** | **String** |  | [optional] 
**x_fant** | **String** |  | [optional] 
**ender** | [**Endereco**](.md) |  | [optional] 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::Empresa.new(cnpj: null,
                                 x_nome: null,
                                 x_fant: null,
                                 ender: null)
```


