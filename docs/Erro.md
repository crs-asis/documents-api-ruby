# DocumentsApi::Erro

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Integer** |  | 
**mensagem** | **String** |  | 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::Erro.new(status: null,
                                 mensagem: null)
```


