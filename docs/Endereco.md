# DocumentsApi::Endereco

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**x_lgr** | **String** |  | [optional] 
**nro** | **String** |  | [optional] 
**x_bairro** | **String** |  | [optional] 
**c_mun** | **Integer** |  | [optional] 
**x_mun** | **String** |  | [optional] 
**uf** | **String** |  | [optional] 
**cep** | **String** |  | [optional] 
**c_pais** | **String** |  | [optional] 
**x_pais** | **String** |  | [optional] 
**fone** | **String** |  | [optional] 

## Code Sample

```ruby
require 'DocumentsApi'

instance = DocumentsApi::Endereco.new(x_lgr: null,
                                 nro: null,
                                 x_bairro: null,
                                 c_mun: null,
                                 x_mun: null,
                                 uf: null,
                                 cep: null,
                                 c_pais: null,
                                 x_pais: null,
                                 fone: null)
```


