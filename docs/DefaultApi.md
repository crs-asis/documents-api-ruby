# DocumentsApi::DefaultApi

All URIs are relative to *http://oraculo.asisprojetos.com.br/api/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**nfe_ch_n_fe_get**](DefaultApi.md#nfe_ch_n_fe_get) | **GET** /nfe/{chNFe} | 
[**nfe_evento_post**](DefaultApi.md#nfe_evento_post) | **POST** /nfe/evento | Registra evento de NFe através do XML
[**nfe_get**](DefaultApi.md#nfe_get) | **GET** /nfe | 
[**nfe_post**](DefaultApi.md#nfe_post) | **POST** /nfe | Registra NFe através do XML



## nfe_ch_n_fe_get

> Object nfe_ch_n_fe_get(ch_n_fe)



Retorna o XML especificado pela chave da NFe

### Example

```ruby
# load the gem
require 'documents_api'
# setup authorization
DocumentsApi.configure do |config|
  # Configure API key authorization: ApiKeyAuth
  config.api_key['X-ORACULO-API-KEY'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['X-ORACULO-API-KEY'] = 'Bearer'
end

api_instance = DocumentsApi::DefaultApi.new
ch_n_fe = 'ch_n_fe_example' # String | Chave da NFe

begin
  result = api_instance.nfe_ch_n_fe_get(ch_n_fe)
  p result
rescue DocumentsApi::ApiError => e
  puts "Exception when calling DefaultApi->nfe_ch_n_fe_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ch_n_fe** | **String**| Chave da NFe | 

### Return type

**Object**

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/xml


## nfe_evento_post

> nfe_evento_post(body)

Registra evento de NFe através do XML

### Example

```ruby
# load the gem
require 'documents_api'
# setup authorization
DocumentsApi.configure do |config|
  # Configure API key authorization: ApiKeyAuth
  config.api_key['X-ORACULO-API-KEY'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['X-ORACULO-API-KEY'] = 'Bearer'
end

api_instance = DocumentsApi::DefaultApi.new
body = nil # Object | 

begin
  #Registra evento de NFe através do XML
  api_instance.nfe_evento_post(body)
rescue DocumentsApi::ApiError => e
  puts "Exception when calling DefaultApi->nfe_evento_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **Object**|  | 

### Return type

nil (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: application/xml
- **Accept**: Not defined


## nfe_get

> Object nfe_get(opts)



Lista NFe's associadas à conta

### Example

```ruby
# load the gem
require 'documents_api'
# setup authorization
DocumentsApi.configure do |config|
  # Configure API key authorization: ApiKeyAuth
  config.api_key['X-ORACULO-API-KEY'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['X-ORACULO-API-KEY'] = 'Bearer'
end

api_instance = DocumentsApi::DefaultApi.new
opts = {
  emit_cnpj: 'emit_cnpj_example', # String | CNPJ do emitente
  dest_cnpj: 'dest_cnpj_example', # String | CNPJ do destinatário
  offset: 56, # Integer | 
  limit: 56 # Integer | 
}

begin
  result = api_instance.nfe_get(opts)
  p result
rescue DocumentsApi::ApiError => e
  puts "Exception when calling DefaultApi->nfe_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **emit_cnpj** | **String**| CNPJ do emitente | [optional] 
 **dest_cnpj** | **String**| CNPJ do destinatário | [optional] 
 **offset** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] 

### Return type

**Object**

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## nfe_post

> nfe_post(body)

Registra NFe através do XML

### Example

```ruby
# load the gem
require 'documents_api'
# setup authorization
DocumentsApi.configure do |config|
  # Configure API key authorization: ApiKeyAuth
  config.api_key['X-ORACULO-API-KEY'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['X-ORACULO-API-KEY'] = 'Bearer'
end

api_instance = DocumentsApi::DefaultApi.new
body = nil # Object | 

begin
  #Registra NFe através do XML
  api_instance.nfe_post(body)
rescue DocumentsApi::ApiError => e
  puts "Exception when calling DefaultApi->nfe_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **Object**|  | 

### Return type

nil (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: application/xml
- **Accept**: Not defined

