=begin
#API Oráculo

#API de documentos fiscais Oráculo

The version of the OpenAPI document: 2.0.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.3.0

=end

require 'cgi'

module DocumentsApi
  class DefaultApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Retorna o XML especificado pela chave da NFe
    # @param ch_n_fe [String] Chave da NFe
    # @param [Hash] opts the optional parameters
    # @return [Object]
    def nfe_ch_n_fe_get(ch_n_fe, opts = {})
      data, _status_code, _headers = nfe_ch_n_fe_get_with_http_info(ch_n_fe, opts)
      data
    end

    # Retorna o XML especificado pela chave da NFe
    # @param ch_n_fe [String] Chave da NFe
    # @param [Hash] opts the optional parameters
    # @return [Array<(Object, Integer, Hash)>] Object data, response status code and response headers
    def nfe_ch_n_fe_get_with_http_info(ch_n_fe, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: DefaultApi.nfe_ch_n_fe_get ...'
      end
      # verify the required parameter 'ch_n_fe' is set
      if @api_client.config.client_side_validation && ch_n_fe.nil?
        fail ArgumentError, "Missing the required parameter 'ch_n_fe' when calling DefaultApi.nfe_ch_n_fe_get"
      end
      # resource path
      local_var_path = '/nfe/{chNFe}'.sub('{' + 'chNFe' + '}', CGI.escape(ch_n_fe.to_s))

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/xml'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'Object' 

      # auth_names
      auth_names = opts[:auth_names] || ['ApiKeyAuth']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: DefaultApi#nfe_ch_n_fe_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Registra evento de NFe através do XML
    # @param body [Object] 
    # @param [Hash] opts the optional parameters
    # @return [nil]
    def nfe_evento_post(body, opts = {})
      nfe_evento_post_with_http_info(body, opts)
      nil
    end

    # Registra evento de NFe através do XML
    # @param body [Object] 
    # @param [Hash] opts the optional parameters
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def nfe_evento_post_with_http_info(body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: DefaultApi.nfe_evento_post ...'
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling DefaultApi.nfe_evento_post"
      end
      # resource path
      local_var_path = '/nfe/evento'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/xml'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] || @api_client.object_to_http_body(body) 

      # return_type
      return_type = opts[:return_type] 

      # auth_names
      auth_names = opts[:auth_names] || ['ApiKeyAuth']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:POST, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: DefaultApi#nfe_evento_post\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Lista NFe's associadas à conta
    # @param [Hash] opts the optional parameters
    # @option opts [String] :emit_cnpj CNPJ do emitente
    # @option opts [String] :dest_cnpj CNPJ do destinatário
    # @option opts [Integer] :offset 
    # @option opts [Integer] :limit 
    # @return [Object]
    def nfe_get(opts = {})
      data, _status_code, _headers = nfe_get_with_http_info(opts)
      data
    end

    # Lista NFe&#39;s associadas à conta
    # @param [Hash] opts the optional parameters
    # @option opts [String] :emit_cnpj CNPJ do emitente
    # @option opts [String] :dest_cnpj CNPJ do destinatário
    # @option opts [Integer] :offset 
    # @option opts [Integer] :limit 
    # @return [Array<(Object, Integer, Hash)>] Object data, response status code and response headers
    def nfe_get_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: DefaultApi.nfe_get ...'
      end
      # resource path
      local_var_path = '/nfe'

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'emitCNPJ'] = opts[:'emit_cnpj'] if !opts[:'emit_cnpj'].nil?
      query_params[:'destCNPJ'] = opts[:'dest_cnpj'] if !opts[:'dest_cnpj'].nil?
      query_params[:'offset'] = opts[:'offset'] if !opts[:'offset'].nil?
      query_params[:'limit'] = opts[:'limit'] if !opts[:'limit'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'Object' 

      # auth_names
      auth_names = opts[:auth_names] || ['ApiKeyAuth']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: DefaultApi#nfe_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Registra NFe através do XML
    # @param body [Object] 
    # @param [Hash] opts the optional parameters
    # @return [nil]
    def nfe_post(body, opts = {})
      nfe_post_with_http_info(body, opts)
      nil
    end

    # Registra NFe através do XML
    # @param body [Object] 
    # @param [Hash] opts the optional parameters
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def nfe_post_with_http_info(body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: DefaultApi.nfe_post ...'
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling DefaultApi.nfe_post"
      end
      # resource path
      local_var_path = '/nfe'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/xml'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] || @api_client.object_to_http_body(body) 

      # return_type
      return_type = opts[:return_type] 

      # auth_names
      auth_names = opts[:auth_names] || ['ApiKeyAuth']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:POST, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: DefaultApi#nfe_post\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
