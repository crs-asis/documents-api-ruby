=begin
#API Oráculo

#API de documentos fiscais Oráculo

The version of the OpenAPI document: 2.0.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.3.0

=end

require 'date'

module DocumentsApi
  class Produto
    # Código do produto
    attr_accessor :c_prod

    # Código de barras EAN
    attr_accessor :c_ean

    # Nome do Produto
    attr_accessor :x_prod

    # Nomenclatura Comum do Mercosul
    attr_accessor :ncm

    # Código Especificador da Substituição Tributária
    attr_accessor :cest

    # Código Fiscal de Operações e Prestações
    attr_accessor :cfop

    # Unidade de medida comercial
    attr_accessor :u_com

    # Quantidade Comercial
    attr_accessor :q_com

    # Valor Unitário Comercial
    attr_accessor :v_un_com

    # Valor total dos produtos
    attr_accessor :v_prod

    # Código de barras EAN
    attr_accessor :c_ean_trib

    # Unidade de medida tributária
    attr_accessor :u_trib

    # Quantidade tributada
    attr_accessor :q_trib

    # Valor unitário tributado
    attr_accessor :v_un_trib

    # Outras despesas acessórias
    attr_accessor :v_outro

    # Indica se o valor do item é computado no valor final da NFe
    attr_accessor :ind_tot

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'c_prod' => :'cProd',
        :'c_ean' => :'cEAN',
        :'x_prod' => :'xProd',
        :'ncm' => :'NCM',
        :'cest' => :'CEST',
        :'cfop' => :'CFOP',
        :'u_com' => :'uCom',
        :'q_com' => :'qCom',
        :'v_un_com' => :'vUnCom',
        :'v_prod' => :'vProd',
        :'c_ean_trib' => :'cEANTrib',
        :'u_trib' => :'uTrib',
        :'q_trib' => :'qTrib',
        :'v_un_trib' => :'vUnTrib',
        :'v_outro' => :'vOutro',
        :'ind_tot' => :'indTot'
      }
    end

    # Attribute type mapping.
    def self.openapi_types
      {
        :'c_prod' => :'Integer',
        :'c_ean' => :'String',
        :'x_prod' => :'String',
        :'ncm' => :'String',
        :'cest' => :'Integer',
        :'cfop' => :'String',
        :'u_com' => :'String',
        :'q_com' => :'Float',
        :'v_un_com' => :'String',
        :'v_prod' => :'Float',
        :'c_ean_trib' => :'String',
        :'u_trib' => :'String',
        :'q_trib' => :'Float',
        :'v_un_trib' => :'Float',
        :'v_outro' => :'Float',
        :'ind_tot' => :'Integer'
      }
    end

    # List of attributes with nullable: true
    def self.openapi_nullable
      Set.new([
      ])
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      if (!attributes.is_a?(Hash))
        fail ArgumentError, "The input argument (attributes) must be a hash in `DocumentsApi::Produto` initialize method"
      end

      # check to see if the attribute exists and convert string to symbol for hash key
      attributes = attributes.each_with_object({}) { |(k, v), h|
        if (!self.class.attribute_map.key?(k.to_sym))
          fail ArgumentError, "`#{k}` is not a valid attribute in `DocumentsApi::Produto`. Please check the name to make sure it's valid. List of attributes: " + self.class.attribute_map.keys.inspect
        end
        h[k.to_sym] = v
      }

      if attributes.key?(:'c_prod')
        self.c_prod = attributes[:'c_prod']
      end

      if attributes.key?(:'c_ean')
        self.c_ean = attributes[:'c_ean']
      end

      if attributes.key?(:'x_prod')
        self.x_prod = attributes[:'x_prod']
      end

      if attributes.key?(:'ncm')
        self.ncm = attributes[:'ncm']
      end

      if attributes.key?(:'cest')
        self.cest = attributes[:'cest']
      end

      if attributes.key?(:'cfop')
        self.cfop = attributes[:'cfop']
      end

      if attributes.key?(:'u_com')
        self.u_com = attributes[:'u_com']
      end

      if attributes.key?(:'q_com')
        self.q_com = attributes[:'q_com']
      end

      if attributes.key?(:'v_un_com')
        self.v_un_com = attributes[:'v_un_com']
      end

      if attributes.key?(:'v_prod')
        self.v_prod = attributes[:'v_prod']
      end

      if attributes.key?(:'c_ean_trib')
        self.c_ean_trib = attributes[:'c_ean_trib']
      end

      if attributes.key?(:'u_trib')
        self.u_trib = attributes[:'u_trib']
      end

      if attributes.key?(:'q_trib')
        self.q_trib = attributes[:'q_trib']
      end

      if attributes.key?(:'v_un_trib')
        self.v_un_trib = attributes[:'v_un_trib']
      end

      if attributes.key?(:'v_outro')
        self.v_outro = attributes[:'v_outro']
      end

      if attributes.key?(:'ind_tot')
        self.ind_tot = attributes[:'ind_tot']
      end
    end

    # Show invalid properties with the reasons. Usually used together with valid?
    # @return Array for valid properties with the reasons
    def list_invalid_properties
      invalid_properties = Array.new
      invalid_properties
    end

    # Check to see if the all the properties in the model are valid
    # @return true if the model is valid
    def valid?
      true
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          c_prod == o.c_prod &&
          c_ean == o.c_ean &&
          x_prod == o.x_prod &&
          ncm == o.ncm &&
          cest == o.cest &&
          cfop == o.cfop &&
          u_com == o.u_com &&
          q_com == o.q_com &&
          v_un_com == o.v_un_com &&
          v_prod == o.v_prod &&
          c_ean_trib == o.c_ean_trib &&
          u_trib == o.u_trib &&
          q_trib == o.q_trib &&
          v_un_trib == o.v_un_trib &&
          v_outro == o.v_outro &&
          ind_tot == o.ind_tot
    end

    # @see the `==` method
    # @param [Object] Object to be compared
    def eql?(o)
      self == o
    end

    # Calculates hash code according to all attributes.
    # @return [Integer] Hash code
    def hash
      [c_prod, c_ean, x_prod, ncm, cest, cfop, u_com, q_com, v_un_com, v_prod, c_ean_trib, u_trib, q_trib, v_un_trib, v_outro, ind_tot].hash
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def self.build_from_hash(attributes)
      new.build_from_hash(attributes)
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def build_from_hash(attributes)
      return nil unless attributes.is_a?(Hash)
      self.class.openapi_types.each_pair do |key, type|
        if type =~ /\AArray<(.*)>/i
          # check to ensure the input is an array given that the attribute
          # is documented as an array but the input is not
          if attributes[self.class.attribute_map[key]].is_a?(Array)
            self.send("#{key}=", attributes[self.class.attribute_map[key]].map { |v| _deserialize($1, v) })
          end
        elsif !attributes[self.class.attribute_map[key]].nil?
          self.send("#{key}=", _deserialize(type, attributes[self.class.attribute_map[key]]))
        end # or else data not found in attributes(hash), not an issue as the data can be optional
      end

      self
    end

    # Deserializes the data based on type
    # @param string type Data type
    # @param string value Value to be deserialized
    # @return [Object] Deserialized data
    def _deserialize(type, value)
      case type.to_sym
      when :DateTime
        DateTime.parse(value)
      when :Date
        Date.parse(value)
      when :String
        value.to_s
      when :Integer
        value.to_i
      when :Float
        value.to_f
      when :Boolean
        if value.to_s =~ /\A(true|t|yes|y|1)\z/i
          true
        else
          false
        end
      when :Object
        # generic object (usually a Hash), return directly
        value
      when /\AArray<(?<inner_type>.+)>\z/
        inner_type = Regexp.last_match[:inner_type]
        value.map { |v| _deserialize(inner_type, v) }
      when /\AHash<(?<k_type>.+?), (?<v_type>.+)>\z/
        k_type = Regexp.last_match[:k_type]
        v_type = Regexp.last_match[:v_type]
        {}.tap do |hash|
          value.each do |k, v|
            hash[_deserialize(k_type, k)] = _deserialize(v_type, v)
          end
        end
      else # model
        DocumentsApi.const_get(type).build_from_hash(value)
      end
    end

    # Returns the string representation of the object
    # @return [String] String presentation of the object
    def to_s
      to_hash.to_s
    end

    # to_body is an alias to to_hash (backward compatibility)
    # @return [Hash] Returns the object in the form of hash
    def to_body
      to_hash
    end

    # Returns the object in the form of hash
    # @return [Hash] Returns the object in the form of hash
    def to_hash
      hash = {}
      self.class.attribute_map.each_pair do |attr, param|
        value = self.send(attr)
        if value.nil?
          is_nullable = self.class.openapi_nullable.include?(attr)
          next if !is_nullable || (is_nullable && !instance_variable_defined?(:"@#{attr}"))
        end
        
        hash[param] = _to_hash(value)
      end
      hash
    end

    # Outputs non-array value in the form of hash
    # For object, use to_hash. Otherwise, just return the value
    # @param [Object] value Any valid value
    # @return [Hash] Returns the value in the form of hash
    def _to_hash(value)
      if value.is_a?(Array)
        value.compact.map { |v| _to_hash(v) }
      elsif value.is_a?(Hash)
        {}.tap do |hash|
          value.each { |k, v| hash[k] = _to_hash(v) }
        end
      elsif value.respond_to? :to_hash
        value.to_hash
      else
        value
      end
    end
  end
end
